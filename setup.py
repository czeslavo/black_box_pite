from setuptools import setup

setup(
    name='black_box',
    version='0.0.1',
    description='logger for planes',
    author='Grzegorz Burzynski',
    author_email='czeslavo@gmail.com',
    packages=['black_box',],
    install_requires=[
        'pytest',
        'matplotlib',
        'click'
    ],
    entry_points={
          'gui_scripts': [
            'fly-gui = scripts.fake_fly_gui:fake_fly_gui',
            'fplot = scripts.flight_plot:flight_plot',
            'fly = scripts.fake_fly:fake_fly'
        ]
    }
)
