import click

from black_box.flight_plotter import Plotter


@click.command()
def flight_plot():
    """Plots data stored in *.log files in a current directory"""

    plotter = Plotter('.')
    plotter.plot('ggplot')
