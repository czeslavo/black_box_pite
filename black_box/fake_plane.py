import datetime
import random


def random_walk(value, min, max, decr, incr, noise=0):
    """Generates random series of numbers - changing smoothly.
    Parameters:
        value (int): number which should be modified
        min (int): minimal value which our number should be
        max (int): max value which our number should be
        decr (int): value which the number is decremented by
        incr (int): value which the number is incremented by
        noise (optional, int): 0.1% chance for +- noise
    Returns:

    """
    if random.choice([True, False]):
        value += random.uniform(0, incr)
    else:
        value -= random.uniform(0, decr)

    randNoise = random.randint(0, 1000)
    if randNoise == 1:
        value += noise
    elif randNoise == 2:
        value -= noise

    if value > max:
        value = max

    if value < min:
        value = min

    return value


class FakePlane:
    """Fake plane.
    Fakes a plane for the purpose of generating naive random data for the flight logger.
    """
    def __init__(self, logger=None):
        """
        Parameters:
            logger (FlightLogger): logger which will log the flight
        """
        self.logger = logger
        self.data = {'velocity': 300.,
                     'altitude': 10.,
                     'engine1_usage': 50.,
                     'engine2_usage': 50.}

        self.generators = [self._generate_altitude, self._generate_engine1_usage,
                           self._generate_engine2_usage, self._generate_velocity]

        self.time = datetime.datetime.now()

    def get_data(self):
        """Generates data for each parameter of the plane with 60Hz frequency and logs it in separate files.
        Returns:
            dictionary of parameters data (i.e. {'altitude': [50, 100, 200], ...})
        """
        for generate in self.generators:
            generate()

        if self.logger:
            self._log_data()

        return self.data

    def _generate_velocity(self):
        """Generates a random velocity using random_walk function."""
        self.data['velocity'] = random_walk(self.data['velocity'], 120, 800, 10, 15)

    def _generate_altitude(self):
        """Generates a random altitude using random_walk function."""
        self.data['altitude'] = random_walk(self.data['altitude'], 0, 7000, 30, 50, 700)

    def _generate_engine1_usage(self):
        """Generates a random engine1 usage using random_walk function."""
        self.data['engine1_usage'] = random_walk(self.data['engine1_usage'], 0, 100, 1, 1.5)

    def _generate_engine2_usage(self):
        """Generates a random engine2 usage using random_walk function."""
        self.data['engine2_usage'] = random_walk(self.data['engine2_usage'], 0, 100, 1, 1.5)

    def _log_data(self):
        """Logs data to separate files.
        Format of data in files:
            time(%Y-%m-%d %H:%M:%S);value
        """
        self.time += datetime.timedelta(seconds=1)

        for parameter in self.data:
            self.logger.getLogger(parameter).info(
                "%s;%.2f" % (self.time.strftime("%Y-%m-%d %H:%M:%S"), self.data[parameter])
            )
