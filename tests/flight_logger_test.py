from black_box.fake_plane import FakePlane
from black_box.flight_logger import FlightLogger
import os

def test_logger():
    parameters = ['velocity', 'altitude', 'engine1_usage', 'engine2_usage']
    path = os.path.dirname(os.path.abspath(__file__)) + '/logs'
    print(path)
    logger = FlightLogger(parameters, path)
    plane = FakePlane(logger=logger)
    plane.get_data()
