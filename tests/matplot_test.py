import time

import matplotlib.pyplot as plt

from black_box.fake_plane import FakePlane
from black_box.flight_logger import FlightLogger


def test_plot():
    try:
        start_time = time.time()

        # make a plane
        parameters = ['velocity', 'altitude', 'engine1_usage', 'engine2_usage']
        logger = FlightLogger(parameters, '/home/czeslavo/tmp')
        plane = FakePlane(logger=logger)

        figure, plots = plt.subplots(4, 1, figsize=(8, 8), dpi=100)
        figure.canvas.mpl_connect('close_event', quit)

        plots[0].set_xlabel('Time [s]')
        plots[0].set_ylabel('velocity [km/h]')

        plots[1].set_xlabel('Time [s]')
        plots[1].set_ylabel('altitude [MAMSL]')

        plots[2].set_xlabel('Time [s]')
        plots[2].set_ylabel('engine1_usage [%]')

        plots[3].set_xlabel('Time [s]')
        plots[3].set_ylabel('engine2_usage [%]')

        figure.tight_layout()
        plt.show(False)
        plt.draw()

        velocity = []
        altitude = []
        engine1_usage = []
        engine2_usage = []
        times = []

        for i in range(1000):
            times.append(time.time() - start_time)

            velocity.append(plane.get_data()['velocity'])
            plots[0].plot(times, velocity, color='b')

            altitude.append(plane.get_data()['altitude'])
            plots[1].plot(times, altitude, color='g')

            engine1_usage.append(plane.get_data()['engine1_usage'])
            plots[2].plot(times, engine1_usage, color='r')

            engine2_usage.append(plane.get_data()['engine2_usage'])
            plots[3].plot(times, engine2_usage, color='c')

            plt.draw()
            plt.pause(0.5)
    except Exception as e:
        pass

