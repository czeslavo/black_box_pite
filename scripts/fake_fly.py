import click

from black_box.fake_plane import FakePlane
from black_box.flight_logger import FlightLogger


@click.command()
@click.argument('path', type=click.Path(exists=True))
@click.argument('secs', type=click.IntRange(1, 7200))
def fake_fly(path, secs):
    """Flies for a given period of time, generating data and storing it in a given path..
    Parameters:
        path (str): path where we would like to store *.log files
        secs (int): time of flying in seconds
    """

    parameters = ['velocity', 'altitude', 'engine1_usage', 'engine2_usage']
    logger = FlightLogger(parameters, path)
    plane = FakePlane(logger=logger)

    for i in range(0, secs):
        plane.get_data()
