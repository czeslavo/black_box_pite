test:
	py.test tests

fly_and_plot: clean
	@mkdir ./tmp
	@fly ./tmp 1500
	@cd ./tmp && fplot

fly: clean
	@mkdir ./tmp
	@fly ./tmp ${t}
	@echo "Flying for ${t}s"

plot:
	@[ -d './tmp' ] && cd ./tmp && fplot || echo "No data to plot"

clean:
	@[ -d './tmp' ] && rm -r './tmp' && echo "Cleaned" || echo "Already clean"

.PHONY: test fly_and_plot plot fly clean