import logging
import os


class FlightLogger:
    """Logs various flight parameters to separate files in a given directory."""

    def __init__(self, flight_parameters, directory):
        """
        Parameters:
            flight_parameters (list of str): list of flight parameters names
            directory (str): path to a directory where the logger should store logs
        """
        if not isinstance(flight_parameters, list):
            raise Exception("Flight parameters passed in a wrong format")

        if not os.path.isdir(directory):
            raise Exception("Wrong directory for flight logger")

        self.loggers = {}

        for parameter in flight_parameters:
            # instantiate a logger for the parameter
            logger = logging.getLogger(parameter)
            logger.setLevel(logging.INFO)

            # instantiate a handler for the parameter
            handler = logging.FileHandler(directory + '/' + parameter + ".log")
            handler.setLevel(logging.INFO)

            # set formatter and add it to the handler
            formatter = logging.Formatter('%(message)s')
            handler.setFormatter(formatter)

            # attach the handler to our logger
            logger.addHandler(handler)

            # send instantiated logger to our loggers
            self.loggers[parameter] = logger


    def getLogger(self, name):
        """Gets logger with a given name.
        Parameters:
            name (str): name of the logger we would like to get
        Returns:
            desired FlightLogger
        Raises:
            Exception if there's no such logger
        """
        if self.loggers[name]:
            return self.loggers[name]
        else:
            raise Exception("No such logger in our system")


